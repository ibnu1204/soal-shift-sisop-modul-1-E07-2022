#!/bin/bash

folder=forensic_log_website_daffainfo_log
filelog="log_website_daffainfo.log"

#2a
if [ ! -d $folder ]
then
    mkdir $folder
else
    rm -r -f $folder
    mkdir $folder
fi

#2b
cat $filelog | awk 'END{print "Rata-rata serangan adalah sebanyak " (NR-1)/12" request per jam " }' >> $folder/ratarata.txt

#2c
cat $filelog | awk '{split ($1,a,":");b[a[1]]++;max=0} END {for (i in b) {if (b[i] > max) {max=b[i]; ip=i}} print "IP yang paling banyak mengakses server adalah: " ip " sebanyak " max " request"}' >> $folder/result.txt

#2d
cat $filelog | awk -v word="curl" '{ i += gsub(word, word) } END { print "Ada "i" request yang menggunakan curl sebagai user-agent" }' >> $folder/result.txt

#2e
cat $filelog | awk -F':|"' '{if($6 == 2)print $2}' | uniq >> $folder/result.txt