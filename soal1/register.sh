#!/bin/bash
check_pass(){
	local lengthpass=${#password}
	local user_loc=/home/tegar/sisop/modul1/users
	time=$(date +%T)
	calendar=$(date +%D)	
	if [[ ! -d "$user_loc" ]]
	then
		mkdir -p $user_loc
	fi
	if grep -q $username "$user_loc/user.txt"
	then 
		echo "User already exist, please try again"
		echo $calendar $time REGISTER:ERROR User already exist >> $user_loc/log.txt
	elif [[ $lengthpass -lt 8 ]]
	then 
		echo "password must have more than 8 characters"
	elif [[ $password == $username ]]
	then
		echo "password can't be same as username"
	elif [[ $password == *[a-zA-Z0-9]* ]]
	then
		echo "password must content at least 1 uppercase, lowercase and number"
	else 
		echo "register success"
		echo $username $password >> $user_loc/user.txt
		echo $calendar $time REGISTER:INFO User $username registered successfully >> $user_loc/log.txt
	fi
}

echo "_________________Han's Laptop Register________________"
echo -n "enter your username: "
read username
echo -n "enter your password: "

read -s  password
check_pass
echo "______________________________________________________"