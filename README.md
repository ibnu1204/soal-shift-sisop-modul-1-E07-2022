# soal-shift-sisop-modul-1-E07-2022
### Anggota Kelompok E07
|Nama|NRP|
|--------------|--------------|
|Tegar Ganang Satrio Priambodo|5025201002|
|Rafael Asi Kristanto Tambunan|5025201168|
|Ahmad Ibnu Malik Rahman|5025201232|

# Soal 1
## Tujuan
Praktikan membuat 2 file yaitu register.sh dan main.sh untuk pengguna agar bisa mengerjakan tugas mencari foto yang diinginkan. Setelah melakukan register, pengguna melakukan login untuk memilih opsi download_pic atau att. Ketika memilih opsi download_pic pengguna dapat memilih n jumlah gambar yang ingin diunduh. Apabila pengguna memilih opsi att akan dihitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.
## Register
Ketika menjalankan file register.sh, pengguna dapat memasukkan username dan password yang nantinya disimpan kedalam file user.txt
Terdapat beberapa kriteria untuk username dan password yang didaftarkan yang dijalankan pada kode register.sh yaitu 
```
#!/bin/bash
check_pass(){
	local lengthpass=${#password}
	local user_loc=/home/tegar/sisop/modul1/users
	time=$(date +%T)
	calendar=$(date +%D)	
	if [[ ! -d "$user_loc" ]]
	then
		mkdir -p $user_loc
	fi
	if grep -q $username "$user_loc/user.txt"
	then 
		echo "User already exist, please try again"
		echo $calendar $time REGISTER:ERROR User already exist >> $user_loc/log.txt
	elif [[ $lengthpass -lt 8 ]]
	then 
		echo "password must have more than 8 characters"
	elif [[ $password == $username ]]
	then
		echo "password can't be same as username"
	elif [[ $password == *[a-zA-Z0-9]* ]]
	then
		echo "password must content at least 1 uppercase, lowercase and number"
	else 
		echo "register success"
		echo $username $password >> $user_loc/user.txt
		echo $calendar $time REGISTER:INFO User $username registered successfully >> $user_loc/log.txt
	fi
}

echo "_________________Han's Laptop Register________________"
echo -n "enter your username: "
read username
echo -n "enter your password: "

read -s  password
check_pass
echo "______________________________________________________"
```

![success](/uploads/63c3a4d17063731fcab0d4ad5c16ebf2/success.png)

1. Username yang sudah pernah terdaftar tidak bisa melakukan register. 
```
if grep -q $username "$user_loc/user.txt"
then 
	echo "User already exist, please try again"
	echo $calendar $time REGISTER:ERROR User already exist >> $user_loc/log.txt
```
Apabila username sudah terdaftar didaftarkan kembali akan mengeluarkan output `User already exist, please try again` dan tercatat sebagai error yang disimpan ke dalam file log.txt `$calendar $time REGISTER:ERROR User already exist >> $user_loc/log.txt`

![exist](/uploads/91fdda9e9e7cbc385d812205bf1b3f6c/exist.png)

2. Password minimal 8 karakter
```
elif [[ $lengthpass -lt 8 ]]
then 
	echo "password must have more than 8 characters"
```
Apabila password kurang dari 8 karakter maka akan mengeluarkan output `password must have more than 8 characters`

![min](/uploads/22bb20a5b7af65483b90fb700b5cc42c/min.png)

3. Password tidak boleh sama dengan username
``` 
elif [[ $password == $username ]]
then
	echo "password can't be same as username"
```
Apabila password yang diinput user sama dengan username maka akan mengeluarkan output `password can't be same as username`

![same](/uploads/21ba29c572a5ae946ce4f47dd9a13523/same.png)

4. Password harus memiliki minimal 1 huruf kapital, 1 huruf kecil dan alphanumeric
```
elif [[ $password == *[a-zA-Z0-9]* ]]
then
	echo "password must content at least 1 uppercase, lowercase and number"
```
untuk mengecek apakah password memiliki minimal 1 huruf kapital, 1 huruf kecil dan alphanumeric dapat digunakan regex `[a-zA-Z0-9]` 
Apabila password yang diinputkan tidak memiliki minimal salah satu kriteria maka akan mengeluarkan output `password must content at least 1 uppercase, lowercase and number`

![regex](/uploads/8c1a4a5468cbf6207768eb87a19142b2/regex.png)

Apabila semua kriteria sudah terpenuhi maka registrasi sukses dan username serta password yang diinput oleh user dapat disimpan pada user.txt serta message pada log tersimpan dengan format `$calendar $time REGISTER: INFO User $username registered successfully`
```
else 
	echo "register success"
	echo $username $password >> $user_loc/user.txt
	echo $calendar $time REGISTER:INFO User $username registered successfully >> $user_loc/log.txt
fi
```
Berikut merupakan isi log.txt

![log](/uploads/8f9df04763d0ab8318a369d6c098a626/log.png)

Berikut merupakan isi user.txt

![user](/uploads/345874d621030f725be6b7237b458b08/user.png)

## Main.sh
Ketika file ini dijalankan pengguna dapat melakukan login dan menjalankan perintah untuk mengunduh gambar dan mengecek berapa kali user melakukan login
```
#!/bin/bash

login(){
	user_cek=$(egrep $username "$user_folder")
	pass_cek=$(egrep $password "$user_folder")
    if [[ ! -f "$user_folder" ]]
	then 
		echo "this username is not registered yet"
	else 
		if [[ -n "$pass_cek" ]] && [[ -n "$user_cek" ]]
		then 
		echo "User Log in successfully"
		echo "$calendar $time LOGIN:INFO user $username logged in" >> $log_folder
		echo "Hello, $username"
		echo "1: Download pic 2: att"
		echo -n "Enter your command : "
		read command
			if [[ $command == 1 ]]
			then 
				download_pic
			elif [[ $command == 2 ]]
			then 
				att
			else 
				echo "Command invalid"
			fi
		else
			echo "Failed to login on $username"
			echo "$calendar $time LOGIN:ERROR Failed to login attemp on user $username" >> $log_folder
		fi
	fi
}
download_pic(){
	echo "number of download: "
	read number
	if [[ ! -f "$pic_folder.zip" ]]
	then
		mkdir -p $pic_folder
		count=0
		zip_file
	else
		unzip -P $password $pic_folder.zip
		rm $pic_folder.zip
		count=$(find $pic_folder -type f | wc -l)
		zip_file
	fi
}
zip_file(){
	for(( i=$count+1; i<=$number+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $pic_folder/PIC_$i.jpg
	done
	zip --password $password -r $pic_folder.zip $pic_folder/
	rm -rf $pic_folder
}
att(){
	if [[ ! -f "$log_folder" ]]
	then
		echo "not found"
	else
		awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' $log_folder
	fi
}
time=$(date +%T)
calendar=$(date +%D)
echo "_________________Han's Laptop Login________________"
echo -n "enter your username: "
read username
echo -n "enter your password: "
read -s  password
pic_folder=$(date +%Y-%m-%D)_$username 
user_folder=/home/tegar/sisop/modul1/users/user.txt
log_folder=/home/tegar/sisop/modul1/users/log.txt
login
```
Saat user memasukkan username dan password akan dilakukan pengecekan apakah username dan password sudah terdaftar pada file **user.txt** melalui pemanggilan fungsi `login`
```
login(){
	user_cek=$(egrep $username "$user_folder")
	pass_cek=$(egrep $password "$user_folder")
    if [[ ! -f "$user_folder" ]]
	then 
		echo "this username is not registered yet"
	else 
		if [[ -n "$pass_cek" ]] && [[ -n "$user_cek" ]]
		then 
		echo "User Log in successfully"
		echo "$calendar $time LOGIN:INFO user $username logged in" >> $log_folder
		echo "Hello, $username"
		echo "1: Download pic 2: att"
		echo -n "Enter your command : "
		read command
			if [[ $command == 1 ]]
			then 
				download_pic
			elif [[ $command == 2 ]]
			then 
				att
			else 
				echo "Command invalid"
			fi
		else
			echo "Failed to login on $username"
			echo "$calendar $time LOGIN:ERROR Failed to login attemp on user $username" >> $log_folder
		fi
	fi
}
```
Apabila username yang dimasukkan tidak terdaftar pada file user.txt maka akan muncul output `this username is not registered yet` dan apabila username dan password terdaftar dan sesuai yang ada pada file user.txt maka akan muncul output `User Log in successfully`dan pada log.txt akan ditulis `$calendar $time LOGIN:INFO user $username logged in`

Jika pengguna memasukkan password yang salah maka akan muncul output `Failed to login on $username` dan pada file log.txt ditulis output `$calendar $time LOGIN:ERROR Failed to login attemp on user $username`

Setelah user berhasil melakukan login maka user dapat memasukkan perintah untuk menjalankan fungsi mengunduh gambar atau att. Apabila user salah memasukkan perintah maka akan muncul output `Command invalid'

Jika user mengisi perintah 1 maka fungsi `download_pic` akan dijalankan
```
download_pic(){
	echo "number of download: "
	read number
	if [[ ! -f "$pic_folder.zip" ]]
	then
		mkdir -p $pic_folder
		count=0
		zip_file
	else
		unzip -P $password $pic_folder.zip
		rm $pic_folder.zip
		count=$(find $pic_folder -type f | wc -l)
		zip_file
	fi
}
```
Pada fungsi ini pengguna memasukkan jumlah gambar yang ingin diunduh kemudian hasil unduhan akan dimasukkan kedalam folder sesuai dengan waktu dan username, dan selanjutnya dijalankan fungsi `zip_file` untuk mendownload dan melakukan zip file.
```
ip_file(){
	for(( i=$count+1; i<=$number+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $pic_folder/PIC_$i.jpg
	done
	zip --password $password -r $pic_folder.zip $pic_folder/
	rm -rf $pic_folder
}
```
Berikut merupakan hasil eksekusi program ketika user memilih untuk mengunduh gambar dan folder isi hasil unduh

![download](/uploads/6286bc8c30d6c9f8078271cbfa3b3229/download.png)

![zip](/uploads/add0950829a189d7818877a88ed68c0f/zip.png)

Apabila user mengisi perintah 2 maka fungsi `att` akan dijalankan. Fungsi `att` dipanggil untuk menghitung jumlah login pengguna
```
att(){
	if [[ ! -f "$log_folder" ]]
	then
		echo "not found"
	else
		awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' $log_folder
	fi
}
```
Berikut merupakan hasil eksekusi program ketika user memilih fungsi `att`

![att](/uploads/5c001885cbd5ea99b2d728cbdab38011/att.png)

## Kendala yang dialami
Kendala saat mengerjakan soal 1 ini yaitu sering terjadi error pada kriteria password harus memiliki minimal 1 huruf kapital, 1 huruf kecil dan alphanumeric awalnya kode kami seperti ini
```
$password != *[a-zA-Z0-9]* 
```
error terjadi ketika user memasukkan password user tetapi bisa register walaupun tidak memiliki minimal 1 huruf kapital, 1 huruf kecil dan alphanumeric
untuk penyelesaiannya kami mengubah kode seperti ini 
```
$password != *[a-zA-Z0-9]* 
```
# Soal 2
## 2 A
Membuat folder bernama `forensic_log_website_daffainfo_log`. Jika foldernya belum ada, akan langsung dibuat folder baru. Namun jika sudah ada, foldernya akan dihapus terlebih dahulu dan dibuat folder baru
```
folder=forensic_log_website_daffainfo_log
if [ ! -d $folder ]
then
    mkdir $folder
else
    rm -r -f $folder
    mkdir $folder
fi
```
Berikut adalah hasilnya setelah program dijalankan

![mkdir](/uploads/14b7230f5420888b1c3a44e67378936a/mkdir.png)

## 2 B
Menggunakan fungsi awk untuk menghitung rata-rata request per jam yang dikirimkan penyerang ke website dan memasukkan jumlah rata-ratanya ke dalam file `ratarata.txt`
```
filelog="log_website_daffainfo.log"
cat $filelog | awk 'END{print "Rata-rata serangan adalah sebanyak " (NR-1)/12" request per jam " }' >> $folder/ratarata.txt
```
Berikut adalah hasil pada file `ratarata.txt` setelah program dijalankan

![rata2](/uploads/51c6ba0340db87d6c97f394ea9c6a205/rata2.png)

## 2 C
Menggunakan fungsi awk untuk menampilkan IP yang paling banyak melakukan request ke server dan berapa banyak request yang dikirimkan dengan IP tersebut. Kemudian outputnya dimasukkan ke dalam file `result.txt`
```
cat $filelog | awk '{split ($1,a,":");b[a[1]]++;max=0} END {for (i in b) {if (b[i] > max) {max=b[i]; ip=i}} print "IP yang paling banyak mengakses server adalah: " ip " sebanyak " max " request"}' >> $folder/result.txt
```
## 2 D
Menggunakan fungsi awk untuk menghitung berapa banyak request yang menggunakan user-agent curl dan memasukkan hasilnya ke dalam file `result.txt`
```
cat $filelog | awk -v word="curl" '{ i += gsub(word, word) } END { print "Ada "i" request yang menggunakan curl sebagai user-agent" }' >> $folder/result.txt
```
## 2 E
Menggunakan fungsi awk untuk menampilkan daftar IP yang mengakses website pada jam 2 pagi dan memasukkan daftar IP tersebut ke dalam file `result.txt`
```
cat $filelog | awk -F':|"' '{if($6 == 2)print $2}' | uniq >> $folder/result.txt
```
Berikut adalah hasil pada file `result.txt` setelah program dijalankan

![2cde](/uploads/bd7f80d52829a79533fcf60ea99a331b/2cde.png)

## Kendala yang dialami
Masih bingung dalam menggunakan fungsi awk, penggunakan BEGIN END

# Soal 3
